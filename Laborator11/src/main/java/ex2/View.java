package ex2;

import javax.swing.*;
import java.awt.*;
import java.util.Vector;

public class View extends JFrame {
    private final int WIDTH_SCREEN = 660;
    private final int HEIGHT_SCREEN = 480;
    private final int WIDTH_COMPONENTS = 140;
    private final int HEIGHT_COMPONENTS = 20;

    private JButton addButton;
    private JButton deleteButton;
    private JButton showProductsButton;
    private JButton editProductButton;

    private JList<Product> productList;
    private JScrollPane scrollPaneList;

    private JTextField nameTextField;
    private JTextField quantityTextField;
    private JTextField priceTextField;
    private JTextField newQtyTextField;

    private JLabel addProductLabel;
    private JLabel nameLabel;
    private JLabel quantityLabel;
    private JLabel priceLabel;
    private JLabel newQtyLabel;

    private Vector<Product> products;

    public View() {
        this.setLayout(null);
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setSize(WIDTH_SCREEN, HEIGHT_SCREEN);
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        this.setLocation(dim.width / 2 - this.getSize().width / 2,
                dim.height / 2 - this.getSize().height / 2);

        init();
    }

    public void init() {

        nameLabel = new JLabel("Name:");
        nameLabel.setBounds(10, 60, WIDTH_COMPONENTS / 2, HEIGHT_COMPONENTS);

        nameTextField = new JTextField();
        nameTextField.setBounds(nameLabel.getX() + WIDTH_COMPONENTS / 2, nameLabel.getY(),
                WIDTH_COMPONENTS, HEIGHT_COMPONENTS);

        quantityLabel = new JLabel("Quantity:");
        quantityLabel.setBounds(nameLabel.getX(), nameLabel.getY() + HEIGHT_COMPONENTS * 2,
                WIDTH_COMPONENTS / 2, HEIGHT_COMPONENTS);

        quantityTextField = new JTextField();
        quantityTextField.setBounds(nameTextField.getX(), nameTextField.getY() + HEIGHT_COMPONENTS * 2,
                WIDTH_COMPONENTS, HEIGHT_COMPONENTS);


        priceLabel = new JLabel("Price:");
        priceLabel.setBounds(quantityLabel.getX(), quantityLabel.getY() + HEIGHT_COMPONENTS * 2,
                WIDTH_COMPONENTS, HEIGHT_COMPONENTS);

        priceTextField = new JTextField();
        priceTextField.setBounds(quantityTextField.getX(), quantityLabel.getY() + HEIGHT_COMPONENTS * 2,
                WIDTH_COMPONENTS, HEIGHT_COMPONENTS);

        addButton = new JButton("Add new product");
        int totalLengthForm = priceTextField.getX() + priceTextField.getWidth() - priceLabel.getX();
        addButton.setBounds(priceLabel.getX(), priceLabel.getY() + HEIGHT_COMPONENTS * 2, totalLengthForm, HEIGHT_COMPONENTS);
        addButton.setFocusPainted(false);

        addProductLabel = new JLabel("ADD PRODUCTS INTO LIST:");
        addProductLabel.setBounds(nameLabel.getX() + 5, nameLabel.getY() - HEIGHT_COMPONENTS * 2, totalLengthForm, HEIGHT_COMPONENTS);

        products = new Vector<>();
        productList = new JList<>();
        productList.setListData(products);
        scrollPaneList = new JScrollPane(productList);
        scrollPaneList.setBounds(350, addProductLabel.getY(), WIDTH_COMPONENTS * 2, HEIGHT_COMPONENTS * 7);

        showProductsButton = new JButton("View all Products");
        showProductsButton.setBounds(scrollPaneList.getX(), scrollPaneList.getY() - HEIGHT_COMPONENTS,
                WIDTH_COMPONENTS * 2, HEIGHT_COMPONENTS);
        showProductsButton.setFocusPainted(false);


        deleteButton = new JButton("Delete Selected Item");
        deleteButton.setBounds(scrollPaneList.getX(), scrollPaneList.getY() + HEIGHT_COMPONENTS * 7,
                WIDTH_COMPONENTS * 2, HEIGHT_COMPONENTS);
        deleteButton.setFocusPainted(false);

        newQtyTextField = new JTextField();
        newQtyTextField.setBounds(addButton.getX(), addButton.getY() + HEIGHT_COMPONENTS * 4
                , WIDTH_COMPONENTS * 2, HEIGHT_COMPONENTS);

        newQtyLabel = new JLabel("New Quantity:");
        newQtyLabel.setBounds(addButton.getX() + 5, addButton.getY() + HEIGHT_COMPONENTS * 2, totalLengthForm, HEIGHT_COMPONENTS);

        editProductButton = new JButton("Edit Selected Item");
        editProductButton.setBounds(newQtyTextField.getX(), newQtyTextField.getY() + HEIGHT_COMPONENTS
                , WIDTH_COMPONENTS * 2, HEIGHT_COMPONENTS);
        editProductButton.setFocusPainted(false);

        this.add(nameLabel);
        this.add(nameTextField);
        this.add(quantityLabel);
        this.add(quantityTextField);
        this.add(priceLabel);
        this.add(priceTextField);
        this.add(addButton);
        this.add(addProductLabel);
        this.add(scrollPaneList);
        this.add(editProductButton);
        this.add(deleteButton);
        this.add(showProductsButton);
        this.add(newQtyTextField);
        this.add(newQtyLabel);
        this.setVisible(true);

    }

    public JButton getAddButton() {
        return addButton;
    }

    public JButton getDeleteButton() {
        return deleteButton;
    }

    public JButton getShowProductsButton() {
        return showProductsButton;
    }

    public JButton getEditProductButton() {
        return editProductButton;
    }

    public JList<Product> getProductList() {
        return productList;
    }

    public JTextField getNameTextField() {
        return nameTextField;
    }

    public JTextField getQuantityTextField() {
        return quantityTextField;
    }

    public JTextField getPriceTextField() {
        return priceTextField;
    }

    public Vector<Product> getProducts() {
        return products;
    }

    public JScrollPane getScrollPaneList() {
        return scrollPaneList;
    }

    public JTextField getNewQtyTextField() {
        return newQtyTextField;
    }
}

