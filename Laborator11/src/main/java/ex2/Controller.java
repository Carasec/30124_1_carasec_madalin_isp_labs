package ex2;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;

public class Controller {
    private Vector<Product> products = new Vector<>();
    private View view = new View();

    public Controller() {
        view.getAddButton().addActionListener(new addFunctionality());
        view.getShowProductsButton().addActionListener(new viewAllProductsFunctionality());
        view.getDeleteButton().addActionListener(new deleteProductFunctionality());
        view.getEditProductButton().addActionListener(new editProductFunctionality());
    }

    private class addFunctionality implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {

            try {
                String st;
                String name = view.getNameTextField().getText();
                int qty = Integer.parseInt(view.getQuantityTextField().getText());
                int price = Integer.parseInt(view.getPriceTextField().getText());

                if (checkString(name)) {
                    st = "Product already exists!";
                } else {
                    view.getProducts().add(new Product(new String(name), qty, price));
                    st = "Product added successfully";
                }
                JOptionPane.showMessageDialog(null, st);

            } catch (Exception Ex) {
                String st = "Check the input textBoxes, one or more of them have an error";
                JOptionPane.showMessageDialog(null, st);
            }
        }

        private boolean checkString(String name) {
            Vector<Product> vector = view.getProducts();
            for (Product product : vector) {
                if (product.equals(name)) {
                    return true;
                }
            }
            return false;
        }
    }

    private class viewAllProductsFunctionality implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            view.getProductList().setListData(view.getProducts());
        }
    }

    private class deleteProductFunctionality implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            String st;
            int indexOfDeleteItem = view.getProductList().getSelectedIndex();

            if (indexOfDeleteItem >= 0) {
                view.getProducts().remove(indexOfDeleteItem);
                view.getProductList().setListData(view.getProducts());
                st = "Product deleted successfully";
            } else {
                st = "No one product selected";
            }
            JOptionPane.showMessageDialog(null, st);
        }
    }

    private class editProductFunctionality implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            String st;
            int indexOfEditItem = view.getProductList().getSelectedIndex();

            try {
                if (indexOfEditItem >= 0) {
                    int newQty = Integer.parseInt(view.getNewQtyTextField().getText());
                    if (newQty >= 0) {
                        view.getProducts().get(indexOfEditItem).setQuantity(newQty);
                        st = "Product edited successfully";
                    } else {
                        st = "Give a positive quantity";
                    }
                } else {
                    st = "No one selected item!";
                }

            } catch (Exception exception) {
                st = "ERROR! Only numbers in the quantity field. TRY AGAIN!";
            }
            JOptionPane.showMessageDialog(null, st);
            view.getProductList().setListData(view.getProducts());
        }
    }
}
