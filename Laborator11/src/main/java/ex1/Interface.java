package ex1;

import javax.swing.*;
import java.awt.*;

public class Interface extends JFrame implements Observer {
    private JTextField textField;

    public Interface(){
        this.setLayout(null);
        this.setSize(new Dimension(250, 220));
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        init();
        this.setVisible(true);

    }

    public void init(){
        this.textField = new JTextField();
        this.textField.setBounds(50,50,115,50);
        this.textField.setEditable(false);
        this.add(textField);
    }
    @Override
    public void update(Object event) {
        textField.setText("Temperature: "+event+" *C");
    }
}
