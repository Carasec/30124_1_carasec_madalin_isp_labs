package ex1;

public class Main {
    public static void main(String[] args) {
        Sensor sensor = new Sensor();
        Interface graphicUI = new Interface();
        sensor.register(graphicUI);
        new Thread(sensor).start();
    }
}

