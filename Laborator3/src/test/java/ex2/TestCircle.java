package ex2;

import org.junit.jupiter.api.Test;

public class TestCircle {
    @Test
    void testConstructor() {
        Circle cerc = new Circle(2);
        assertEquals(5, cerc.getRadius());
    }

    @Test
    void testArea() {
        Circle cerc1 = new Circle(5, "red");
        assertEquals(3.14 * 5 * 5, 3.14 * cerc1.getRadius() * cerc1.getRadius());

    }

    private void assertEquals(double v, double v1) {
    }

}
