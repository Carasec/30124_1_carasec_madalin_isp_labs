package ex5;

public class Flower {
    int petal;

    private static int counting = 0;

    public Flower() {
        System.out.print("Flower has been created!");
        counting++;
    }

    public int numberOfConstr() {
        return counting;
    }
}
