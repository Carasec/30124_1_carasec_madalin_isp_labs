package ex1;

import java.util.Scanner;

public class Robot {
    private int x;

    public Robot() {
        this.x = 1;
    }

    public void change(int k) {
        if (k >= 1)
            this.x += k;
    }

    public int getX() {
        return x;
    }

    @Override
    public String toString() {
        return "Robot{" +
                "x=" + x +
                '}';
    }
}
