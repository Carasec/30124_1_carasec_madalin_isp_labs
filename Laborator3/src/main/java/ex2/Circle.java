package ex2;

public class Circle {
    private
    double radius = 1.0;
    private
    String color = "red";

    public double getRadius() {
        return radius;
    }

    public double getArea() {
        return 3.14 * radius * radius;
    }

    public Circle(double radius) {

    }

    public Circle(String color) {

    }

    public Circle(double radius, String color) {

    }

}
