package ex2;

import ex1.BankAccount;

import java.util.*;
import java.util.Comparator;

public class Bank {

    private List<BankAccount> accounts = new ArrayList<BankAccount>();


    public void addAccount(String owner, double balance) {

        accounts.add(new BankAccount(owner, balance));

    }

    public void printAccounts() {

        for (int i = 0; i < accounts.size(); i++) {
            System.out.println(accounts.get(i).toString());
        }
    }

    public void printAccounts(double minBalance, double maxBalance) {

        System.out.println("Elements in the range" + minBalance + " and " + maxBalance);
        for (int i = 0; i < accounts.size(); i++) {
            if ((accounts.get(i).getBalance() >= minBalance) && (accounts.get(i).getBalance() <= maxBalance))
                System.out.println(accounts.get(i).toString());
        }

    }

    public BankAccount getAccount(String owner) {

        System.out.println("Search by owner");
        for (int i = 0; i < accounts.size(); i++) {
            if ((accounts.get(i).getOwner() == owner))
                return accounts.get(i);
        }
        return null;//caz in care nu este owner
    }

    public List<BankAccount> getAllAccounts() {

        return this.accounts;

    }

    static class SortByOwner implements Comparator<BankAccount> {
        @Override
        public int compare(BankAccount a, BankAccount b) {
            return a.getOwner().compareTo(b.getOwner());
        }
    }


}
