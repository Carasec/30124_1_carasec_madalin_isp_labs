package ex3;

public class Counters extends Thread {

    Counters(String name) {
        super(name);
    }

    public void run() {
        int i = 0;
        while (++i < 100) {
            System.out.println(getName() + " i = " + i);
            try {
                Thread.sleep((int) (100));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println("First counter's job finalised..");
        while (++i < 200) {
            System.out.println(getName() + " i = " + i);
            try {
                Thread.sleep((int) (200));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println("Second counter's job finalised.");

    }



    public static void main(String[] args) {
        Counters c1 = new Counters("counter1");
        c1.start();

    }
}



