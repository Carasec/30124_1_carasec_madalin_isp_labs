package ex6;


public class Start {

    public boolean start = true;

    private static int minutes;
    private static int seconds;
    private static int miliseconds;


    Start() {

        Start.miliseconds = 0;
        Start.seconds = 0;
        Start.minutes = 0;

    }

    synchronized void startChronometer() {
        try {
            while (start) {
                Thread.sleep(0, 1);
                miliseconds++;
                if (miliseconds == 1001) {
                    miliseconds = 0;
                    seconds++;
                }
                if (seconds == 61) {

                    seconds = 0;
                    minutes++;

                }

                Chronometer.display.setText(minutes + ":" + seconds + ":" + miliseconds);
            }
        } catch (Exception e) {
            System.out.println(e);
        }


    }

    public static void resetChronometer() {

        Start.miliseconds = 0;
        Start.seconds = 0;
        Start.minutes = 0;
    }

}
