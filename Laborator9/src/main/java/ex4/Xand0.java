package ex4;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Xand0 {
    private JButton button1;
    private JButton button2;
    private JButton button3;
    private JButton button4;
    private JButton button5;
    private JButton button6;
    private JButton button7;
    private JButton button8;
    private JButton button9;
    private JPanel gamepanel;
    private JTextField textField1;
    private int count = 1;


    public Xand0() {
        button1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (count % 2 == 1)
                    button1.setText("X");
                else
                    button1.setText("0");
                textField1.setEnabled(false);
                // textField1.setText("sug");
                button1.setEnabled(false);
                count++;
                verificare();
            }
        });
        button2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (count % 2 == 1)
                    button2.setText("X");
                else
                    button2.setText("0");
                button2.setEnabled(false);
                count++;
                verificare();
            }
        });
        button3.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (count % 2 == 1)
                    button3.setText("X");
                else
                    button3.setText("0");
                button3.setEnabled(false);
                count++;
                verificare();
            }
        });
        button4.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (count % 2 == 1)
                    button4.setText("X");
                else
                    button4.setText("0");
                button4.setEnabled(false);
                count++;
                verificare();
            }
        });
        button6.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (count % 2 == 1)
                    button6.setText("X");
                else
                    button6.setText("0");
                button6.setEnabled(false);
                count++;
                verificare();
            }
        });
        button7.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (count % 2 == 1)
                    button7.setText("X");
                else
                    button7.setText("0");
                button7.setEnabled(false);
                count++;
                verificare();
            }
        });
        button5.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (count % 2 == 1)
                    button5.setText("X");
                else
                    button5.setText("0");
                button5.setEnabled(false);
                count++;
                verificare();
            }
        });
        button8.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (count % 2 == 1)
                    button8.setText("X");
                else
                    button8.setText("0");
                button8.setEnabled(false);
                count++;
                verificare();
            }
        });
        button9.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (count % 2 == 1)
                    button9.setText("X");
                else
                    button9.setText("0");
                button9.setEnabled(false);
                count++;
                verificare();
            }
        });
    }


    public void endgame() {
        button1.setEnabled(false);
        button2.setEnabled(false);
        button3.setEnabled(false);
        button4.setEnabled(false);
        button5.setEnabled(false);
        button6.setEnabled(false);
        button7.setEnabled(false);
        button8.setEnabled(false);
        button9.setEnabled(false);
    }

    public void verificare() {
        //verificare X
        if (button1.getText() == "X" && button2.getText() == "X" && button3.getText() == "X") {
            textField1.setText("X castiga");
            endgame();
        } else if (button4.getText() == "X" && button5.getText() == "X" && button6.getText() == "X") {
            textField1.setText("X castiga");
            endgame();
        } else if (button7.getText() == "X" && button8.getText() == "X" && button9.getText() == "X") {
            textField1.setText("X castiga");
            endgame();
        } else if (button1.getText() == "X" && button5.getText() == "X" && button9.getText() == "X") {
            textField1.setText("X castiga");
            endgame();
        } else if (button3.getText() == "X" && button5.getText() == "X" && button7.getText() == "X") {
            textField1.setText("X castiga");
            endgame();
        } else if (button1.getText() == "X" && button4.getText() == "X" && button7.getText() == "X") {
            textField1.setText("X castiga");
            endgame();
        } else if (button2.getText() == "X" && button5.getText() == "X" && button8.getText() == "X") {
            textField1.setText("X castiga");
            endgame();
        } else if (button3.getText() == "X" && button6.getText() == "X" && button9.getText() == "X") {
            textField1.setText("X castiga");
            endgame();
        }

        //verificare O

        if (button1.getText() == "0" && button2.getText() == "0" && button3.getText() == "0") {
            textField1.setText("0 castiga");
            endgame();
        } else if (button4.getText() == "0" && button5.getText() == "0" && button6.getText() == "0") {
            textField1.setText("0 castiga");
            endgame();
        } else if (button7.getText() == "0" && button8.getText() == "0" && button9.getText() == "0") {
            textField1.setText("0 castiga");
            endgame();
        } else if (button1.getText() == "0" && button5.getText() == "0" && button9.getText() == "0") {
            textField1.setText("0 castiga");
            endgame();
        } else if (button3.getText() == "0" && button5.getText() == "0" && button7.getText() == "0") {
            textField1.setText("0 castiga");
            endgame();
        } else if (button1.getText() == "0" && button4.getText() == "0" && button7.getText() == "0") {
            textField1.setText("0 castiga");
            endgame();
        } else if (button2.getText() == "0" && button5.getText() == "0" && button8.getText() == "0") {
            textField1.setText("0 castiga");
            endgame();
        } else if (button3.getText() == "0" && button6.getText() == "0" && button9.getText() == "0") {
            textField1.setText("0 castiga");
            endgame();
        }
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("XandO");
        frame.setContentPane(new Xand0().gamepanel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);

    }
}


