package ex5;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class RailwaySimulator extends JFrame {
    private JPanel RailwaySimulator;
    private JTextField textField1;
    private JTextField textField4;
    private JButton button1;
    private JLabel label1;
    private JLabel label2;
    private JLabel label3;
    private JTextArea textArea1;
    private JTextArea textArea2;
    private JTextArea textArea3;
    private JTextArea textArea4;
    private JTextArea textArea5;
    private JTextArea textArea6;
    private JTextArea textArea7;
    private JTextArea textArea8;
    private JTextArea textArea9;
    private JLabel label4;
    private JLabel label5;
    private JTextField textField2;
    private JLabel label6;
    private JTextField textField3;
    Controler c1 = new Controler("Cluj-Napoca");
    Segment s1 = new Segment(1);
    Segment s2 = new Segment(2);
    Segment s3 = new Segment(3);
    Controler c2 = new Controler("Bucuresti");
    Segment s4 = new Segment(4);
    Segment s5 = new Segment(5);
    Segment s6 = new Segment(6);
    Controler c3 = new Controler("Oradea");
    Segment s7 = new Segment(7);
    Segment s8 = new Segment(8);
    Segment s9 = new Segment(9);

    Train t1 = new Train("Bucuresti", "IC-001");
    Train t2 = new Train("Cluj-Napoca", "R-002");
    Train t3 = new Train("Oradea", "IR-1745");

    RailwaySimulator() {
        button1.setText("Add a train");
        label4.setText("Destination station:");
        label5.setText("Segment of the station:");
        label6.setText("Train:");
        button1.addActionListener(new Button());
        init();

        for (int i = 0; i < 4; i++) {
            c1.controlStep();
            c2.controlStep();
            c3.controlStep();

        }
        if (s1.hasTrain()) {
            RailwaySimulator.this.textArea1.append(" " + s1.getTrain().name);
        }
        if (s2.hasTrain()) {
            RailwaySimulator.this.textArea2.append(" " + s2.getTrain().name);
        }
        if (s3.hasTrain()) {
            RailwaySimulator.this.textArea3.append(" " + s3.getTrain().name);
        }
        if (s4.hasTrain()) {
            RailwaySimulator.this.textArea4.append(" " + s4.getTrain().name);
        }
        if (s5.hasTrain()) {
            RailwaySimulator.this.textArea5.append(" " + s5.getTrain().name);
        }
        if (s6.hasTrain()) {
            RailwaySimulator.this.textArea6.append(" " + s6.getTrain().name);
        }
        if (s7.hasTrain()) {
            RailwaySimulator.this.textArea7.append(" " + s7.getTrain().name);
        }
        if (s8.hasTrain()) {
            RailwaySimulator.this.textArea8.append(" " + s8.getTrain().name);
        }
        if (s9.hasTrain()) {
            RailwaySimulator.this.textArea1.append(" " + s9.getTrain().name);
        }

    }

    public void init() {

        label2.setText("Gara Cluj-Napoca");
        c1.addControlledSegment(s1);
        c1.addControlledSegment(s2);
        c1.addControlledSegment(s3);
        textArea1.setText("Segment 1:");
        textArea2.setText("Segment 2:");
        textArea3.setText("Segment 3:");
        label3.setText("Gara Bucuresti");
        textArea4.setText("Segment 4:");
        textArea5.setText("Segment 5:");
        textArea6.setText("Segment 6:");
        c2.addControlledSegment(s4);
        c2.addControlledSegment(s5);
        c2.addControlledSegment(s6);
        label1.setText("Gara Oradea");
        textArea7.setText("Segment 7:");
        textArea8.setText("Segment 8:");
        textArea9.setText("Segment 9:");
        c3.addControlledSegment(s7);
        c3.addControlledSegment(s8);
        c3.addControlledSegment(s9);
        c1.setNeighbourController(c2);
        c1.setNeighbourController(c3);
        c2.setNeighbourController(c1);
        c2.setNeighbourController(c3);
        c3.setNeighbourController(c1);
        c3.setNeighbourController(c2);
        s1.arriveTrain(t1);
        s9.arriveTrain(t2);
        s5.arriveTrain(t3);
    }

    class Button implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            String destinationName = textField1.getText();
            String segmentNumber = textField4.getText();
            String trainName = textField2.getText();
            Train train4 = new Train(destinationName, trainName);
            switch (destinationName) {
                case "Cluj-Napoca":
                    switch (segmentNumber) {
                        case "1":
                            s1.arriveTrain(train4);
                            RailwaySimulator.this.textArea1.append(" " + trainName);
                            break;
                        case "2":
                            s2.arriveTrain(train4);
                            RailwaySimulator.this.textArea2.append(" " + trainName);
                            break;
                        case "3":
                            s3.arriveTrain(train4);
                            RailwaySimulator.this.textArea3.append(" " + trainName);
                            break;
                        default:
                            label5.setText("Error. This segment does not exist");
                    }
                    break;
                case "Bucuresti":
                    switch (segmentNumber) {
                        case "4":
                            s4.arriveTrain(train4);
                            RailwaySimulator.this.textArea4.append(" " + trainName);
                            break;
                        case "5":
                            s5.arriveTrain(train4);
                            RailwaySimulator.this.textArea5.append(" " + trainName);
                            break;
                        case "6":
                            s6.arriveTrain(train4);
                            RailwaySimulator.this.textArea6.append(" " + trainName);
                            break;
                        default:
                            label5.setText("Error. This segment does not exist");
                    }
                    break;
                case "Oradea":
                    switch (segmentNumber) {
                        case "7":
                            s7.arriveTrain(train4);
                            RailwaySimulator.this.textArea7.append(" " + trainName);
                            break;
                        case "8":
                            s8.arriveTrain(train4);
                            RailwaySimulator.this.textArea8.append(" " + trainName);
                            break;
                        case "9":
                            s9.arriveTrain(train4);
                            RailwaySimulator.this.textArea9.append(" " + trainName);
                            break;
                        default:
                            label5.setText("Error. This segment does not exist");
                    }
                    break;
                default:
                    label4.setText("This station does not exist");
                    break;
            }

        }
    }

    class Controler {

        String stationName;

        ArrayList<Controler> neighbourController = new ArrayList<>();

        //storing station train track segments
        ArrayList<Segment> list = new ArrayList<>();

        public Controler(String gara) {
            stationName = gara;
        }

        void setNeighbourController(Controler v) {
            neighbourController.add(v);
        }

        void addControlledSegment(Segment s) {
            list.add(s);
        }

        int getFreeSegmentId() {
            for (Segment s : list) {
                if (!s.hasTrain())
                    return s.id;
            }
            return -1;
        }

        void controlStep() {
            //check which train must be sent

            for (Segment segment : list) {
                if (segment.hasTrain()) {
                    Train t = segment.getTrain();

                    for (Controler controler : neighbourController) {

                        if (t.getDestination().equals(controler.stationName)) {
                            //check if there is a free segment
                            int id = controler.getFreeSegmentId();
                            if (id == -1) {
                                System.out.println("Trenul +" + t.name + "din gara " + stationName + " nu poate fi trimis catre " + controler.stationName + ". Nici un segment disponibil!");
                                return;
                            }
                            //send train
                            System.out.println("Trenul " + t.name + " pleaca din gara " + stationName + " spre gara " + controler.stationName);
                            segment.departTRain();
                            controler.arriveTrain(t, id);
                        }

                    }
                }
            }//.for

        }//.

        public void arriveTrain(Train t, int idSegment) {
            for (Segment segment : list) {
                //search id segment and add train on it
                if (segment.id == idSegment)
                    if (segment.hasTrain()) {
                        System.out.println("CRASH! Train " + t.name + " colided with " + segment.getTrain().name + " on segment " + segment.id + " in station " + stationName);
                        return;
                    } else {
                        System.out.println("Train " + t.name + " arrived on segment " + segment.id + " in station " + stationName);
                        segment.arriveTrain(t);

                        return;
                    }
            }

            //this should not happen
            System.out.println("Train " + t.name + " cannot be received " + stationName + ". Check controller logic algorithm!");

        }

    }

    class Segment {
        int id;
        Train train;

        Segment(int id) {
            this.id = id;
        }

        boolean hasTrain() {
            return train != null;
        }

        Train departTRain() {
            Train tmp = train;
            this.train = null;
            return tmp;
        }

        void arriveTrain(Train t) {
            train = t;
        }

        Train getTrain() {
            return train;
        }
    }

    class Train {
        String destination;
        String name;

        public Train(String destinatie, String nume) {
            super();
            this.destination = destinatie;
            this.name = nume;
        }

        String getDestination() {
            return destination;
        }

    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("RailwaySimulator");
        frame.setContentPane(new RailwaySimulator().RailwaySimulator);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
