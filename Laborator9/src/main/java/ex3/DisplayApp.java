package ex3;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputMethodEvent;
import java.awt.event.InputMethodListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

public class DisplayApp<FileContent> {
    private JPanel panel;
    private JButton button1;
    private JTextField textField1;
    private JTextPane textPane1;
    private String text2;


    public DisplayApp() {
        button1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              String file=textField1.getText();
              try
              {
                  BufferedReader bf= new BufferedReader(new FileReader(new File(file)));
                  String l= " ";
                  String txt=" ";
                  StringBuilder content=new StringBuilder();
                  l= bf.readLine();;
                  while(l!=null){
                      txt=txt+" "+l+"\n";
                      content.append(l);
                      content.append("\n");
                      l=bf.readLine();
                  }
                  textPane1.setText(content.toString());
              }
              catch (Exception ex){

              }
            }
        });
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("Ex3");
        frame.setContentPane(new DisplayApp().panel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}


