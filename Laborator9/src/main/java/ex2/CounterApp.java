package ex2;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class CounterApp {
    private JPanel asd;
    private JTextField textField1;
    private JButton button1;
    private int counter = 0;

    public CounterApp() {
        button1.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                textField1.setText(counter + " ");
                counter++;

            }
        });
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("CounterApp");
        frame.setContentPane(new CounterApp().asd);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}






