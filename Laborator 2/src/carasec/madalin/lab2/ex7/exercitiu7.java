package carasec.madalin.lab2.ex7;

import java.util.Random;
import java.util.Scanner;

public class exercitiu7 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int nr;
        System.out.print("Guess the number:");
        Random ran = new Random();
        int x = ran.nextInt(10);
        int guessed = 0;
        int guess = 0;
        while (guessed == 0 && guess < 3) {
            guess++;
            nr = in.nextInt();
            if (nr < x)
                System.out.print("Wrong answer.Number is too low\n");
            else if (nr > x)
                System.out.print("Wrong answer.Number too high\n");
            else if (nr == x) {
                System.out.print("Correct!");
                guessed = 1;
            }
        }
    }
}
