package carasec.madalin.lab2.ex6;

import java.util.Scanner;

public class exercitiu6 {
    static int fact(int n) {
        int i = 1;
        for (int p = 1; p <= n; p++)
            i = i * p;
        return i;
    }

    static int factR(int n) {
        if (n == 1)
            return 1;
        else return n * fact(n - 1);
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.print("n=");
        int n = in.nextInt();
        System.out.println("non-recursive: " + fact(n));
        System.out.print("recursive: " + fact(n));
    }
}
