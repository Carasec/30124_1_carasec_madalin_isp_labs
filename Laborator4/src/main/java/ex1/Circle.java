package ex1;

public class Circle {

    private
    double radius = 1.0;
    private
    String color = "red";

    public double getRadius() {
        return radius;
    }

    public String getColor() {
        return color;
    }

    public String setColor(String blue) {
        return getColor();
    }

    public double getArea() {
        return 3.14 * radius * radius;
    }

    public Circle(double radius) {
        this.radius = radius;
    }

    public Circle() {

    }

    public Circle(double radius, String color) {
        this.radius = radius;
        this.color = color;
    }
}
