package ex3;

import ex2.Author;

public class Book {
    private String name;
    private Author author;
    private double price;
    private int qtyIntStock = 0;

    public Book(String name, Author author, double price) {
        this.name = name;
        this.author = author;
        this.price = price;

    }

    public Book(String name, Author author, double price, int qtyIntStock) {
        this.name = name;
        this.author = author;
        this.price = price;
        this.qtyIntStock = qtyIntStock;
    }

    public String getName() {
        return name;
    }

    public Author getAuthor() {
        return author;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getQtyIntStock() {
        return qtyIntStock;
    }

    public void setQtyIntStock(int qtyIntStock) {
        this.qtyIntStock = qtyIntStock;
    }

    @Override
    public String toString() {
        return
                "~" + name + '\'' +
                        "~ by" + author + "author" + author.getGender() + "email" + author.getEmail();
    }
}
