package ex4;

import ex2.Author;

import java.util.Arrays;

public class Book2 {
    private String name;
    private Author[] authors;
    private double price;
    private int qtyIntStock = 0;

    public Book2(String name, Author[] authors, double price) {
        this.name = name;
        this.authors = authors;
        this.price = price;

    }

    public Book2(String name, Author[] authors, double price, int qtyIntStock) {
        this.name = name;
        this.authors = authors;
        this.price = price;
        this.qtyIntStock = qtyIntStock;
    }

    public String getName() {
        return name;
    }

    public Author[] getAuthors() {
        return authors;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getQtyIntStock() {
        return qtyIntStock;
    }

    public void setQtyIntStock(int qtyIntStock) {
        this.qtyIntStock = qtyIntStock;
    }

    @Override
    public String toString() {
        return
                ("~" + name + '\'' +
                        "~ by" + authors.length +
                        "authors");
    }

    public void printAuthors() {
        String content = "The authors of the books names" + getName() + "~ are: ";
        for (Author tempAuthor : authors) {
            content = content.concat(" " + tempAuthor.getName() + "");
        }
        System.out.println(content);
    }
}
