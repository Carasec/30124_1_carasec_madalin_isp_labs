package ex6;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestShape {
    @Test
    void TestConstructor() {
        Shape shape = new Shape();
        assertEquals("red", shape.getColor());
        assertEquals(true, shape.isFilled());
    }
}
