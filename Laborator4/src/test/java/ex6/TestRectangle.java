package ex6;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestRectangle {
    @Test
    void TestConstructor() {
        Rectangle rectangle = new Rectangle();
        assertEquals(1, rectangle.getLength());
        assertEquals(1, rectangle.getWidth());
    }

    @Test
    void TestConstructor2() {
        Rectangle rectangle = new Rectangle(3, 5);
        assertEquals(3, rectangle.getWidth());
        assertEquals(5, rectangle.getLength());
    }

    @Test
    void TestConstructor3() {
        Rectangle rectangle = new Rectangle(2, 4, "blue", true);
        assertEquals(2, rectangle.getWidth());
        assertEquals(4, rectangle.getLength());
        assertEquals("blue", rectangle.getColor());
        assertEquals(true, rectangle.isFilled());
    }

    @Test
    void TestGetArea() {
        Rectangle rectangle = new Rectangle(3, 4);
        assertEquals(12.0, rectangle.GetArea());
    }

    @Test
    void TestGetPerimeter() {
        Rectangle rectangle = new Rectangle(3, 6);
        assertEquals(18, rectangle.GetPerimeter());
    }
}
