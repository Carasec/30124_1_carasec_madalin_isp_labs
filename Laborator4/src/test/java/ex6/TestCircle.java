package ex6;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestCircle {
    @Test
    void TestConstructor() {
        Circle circle = new Circle();
        assertEquals(1, circle.getRadius());
        assertEquals("red", circle.getColor());
        assertEquals(true, circle.isFilled());
    }

    @Test
    void TestConstructor2() {
        Circle circle = new Circle(3);
        assertEquals(3, circle.getRadius());
        assertEquals("red", circle.getColor());
        assertEquals(true, circle.isFilled());
    }

    @Test
    void TestConstructor3() {
        Circle circle = new Circle(3, "blue", true);
        assertEquals(3, circle.getRadius());
        assertEquals("blue", circle.getColor());
        assertEquals(true, circle.isFilled());
    }

    @Test
    void TestGetArea() {
        Circle circle = new Circle(3, "yellow", false);
        assertEquals(28.274333882308138, circle.GetArea(), 0.001);
    }

    @Test
    void TestGetPerimeter() {
        Circle circle = new Circle();
        assertEquals(6.283185307179586, circle.GetPerimeter(), 0.001);
    }
}

