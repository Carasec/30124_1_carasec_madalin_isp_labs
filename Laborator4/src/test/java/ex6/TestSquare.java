package ex6;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestSquare {
    @Test
    void TestConstructor() {
        Square square = new Square();
        assertEquals(1, square.GetSide());
        assertEquals(1, square.GetSide());
    }

    @Test
    void TestConstructor2() {
        Square square = new Square(3);
        assertEquals(3, square.GetSide());
        assertEquals(3, square.GetSide());
    }

    @Test
    void TestConstructor3() {
        Square square = new Square(3, "green", true);
        assertEquals(3, square.GetSide());
        assertEquals(3, square.GetSide());
        assertEquals("green", square.getColor());
        assertEquals(true, square.isFilled());
    }
}
