package ex4;

import ex2.Author;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestBook2 {
    Author author = new Author("Robert Kyosaki", "robertkyosaki@yahoo.com", 'm');
    Author author2 = new Author("Camil Petrescu", "CPetrescu@gmail.com", 'm');
    Author author3 = new Author("Ana Blandiana", "Blandiana@yahoo.com", 'f');
    Author author4 = new Author("Eugen Barbu", "Barbu20@yahoo.com", 'm');

    Author[] authors = {author, author2};
    Author[] authors2 = {author3, author4};


    @Test
    void TestConstructor() {
        Book2 book = new Book2("Tainele vietii", authors, 100);
        assertEquals("Tainele vietii", book.getName());
        assertEquals(authors, book.getAuthors());
        assertEquals(100, book.getPrice());

    }

    @Test
    void TestConstructor2() {
        Book2 book2 = new Book2("Povesti nemuritoare", authors2, 50, 500);
        assertEquals("Povesti nemuritoare", book2.getName());
        assertEquals(authors2, book2.getAuthors());
        assertEquals(50, book2.getPrice());
        assertEquals(500, book2.getQtyIntStock());
    }
}
