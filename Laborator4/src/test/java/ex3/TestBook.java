package ex3;

import ex2.Author;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestBook {
    Author author = new Author("Ana Blandiana", "Blandiana@yahoo.com", 'f');
    Author author2 = new Author("Robert Kyosaki", "robertkyosaki@yahoo.com", 'm');

    @Test
    void TestContructor() {
        Book book = new Book("Cuplu", author, 50);
        assertEquals("Cuplu", book.getName());
        assertEquals("Ana Blandiana", book.getAuthor().getName());
        assertEquals("Blandiana@yahoo.com", book.getAuthor().getEmail());
        assertEquals('f', book.getAuthor().getGender());
    }

    @Test
    void TestConstructor2() {
        Book book = new Book("Tata bogat, tata sarac", author2, 30, 120);
        assertEquals("Tata bogat, tata sarac", book.getName());
        assertEquals("Robert Kyosaki", book.getAuthor().getName());
        assertEquals("robertkyosaki@yahoo.com", book.getAuthor().getEmail());
        assertEquals('m', book.getAuthor().getGender());
        assertEquals(30, book.getPrice());
    }
}
