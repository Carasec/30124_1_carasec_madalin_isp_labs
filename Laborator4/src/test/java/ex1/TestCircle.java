package ex1;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestCircle {
    @Test
    void TestConstructor() {
        Circle circle = new Circle();
        assertEquals(1.0, circle.getRadius());
        assertEquals("red", circle.getColor());
    }

    @Test
    void TestConstructor2() {
        Circle circle = new Circle(2.0, "blue");
        assertEquals(2.0, circle.getRadius());
        assertEquals("blue", circle.getColor());
    }

    @Test
    void TestGetArea() {
        Circle circle = new Circle();
        assertEquals(3.14, circle.getArea(), 0.001);
    }


}
