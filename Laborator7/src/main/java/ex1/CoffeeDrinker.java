package ex1;

public class CoffeeDrinker {
    void CofeeDrinker(Cofee c) throws CofeeTemperatureException, CofeeConcentrationException {
        if (c.getTemp() > 50)
            throw new CofeeTemperatureException(c.getTemp(), "Cofee is to hot!");
        if (c.getConc() > 35)
            throw new CofeeConcentrationException(c.getConc(), "Cofee concentration to high!");
        System.out.println("Drink cofee:" + c);
    }
}
