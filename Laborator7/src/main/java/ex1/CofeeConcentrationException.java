package ex1;

public class CofeeConcentrationException extends Throwable {
    int c;

    public CofeeConcentrationException(int c, String msg) {
        super(msg);
        this.c = c;
    }

    int getConc() {
        return c;
    }
}
