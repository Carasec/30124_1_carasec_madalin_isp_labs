package ex1;

public class Cofee {
    private int temp;
    private int conc;
    private static int nrOfInstances;

    Cofee(int t, int c) {
        temp = t;
        conc = c;
        nrOfInstances++;
    }

    public static int getNrOfInstances() {
        return nrOfInstances;
    }

    int getTemp() {
        return temp;
    }

    int getConc() {
        return conc;
    }

    public String toString() {
        return "[cofee temperature=" + temp + ":concentration=" + conc + "]";
    }
}

