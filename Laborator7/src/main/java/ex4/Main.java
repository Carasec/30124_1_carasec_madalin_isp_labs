package ex4;

import java.io.*;

public class Main {
    private String fileToStoreObject = "src/main/java/ex4/cars.txt";

    public static void main(String[] args) {
        Main main = new Main();
        Car car = new Car("Mercedes-Benz GLE", 120000);
        main.serializeCar(car);
        System.out.println("Object serialized: " + car);
        System.out.println("Object deserialized: " + main.deserializeCar());
        Car car1 = new Car("BMW E60 M5", 25000);
        main.serializeCar(car1);
        System.out.println("Object serialized: " + car1);
        System.out.println("Object deserialized: " + main.deserializeCar());

    }

    private void serializeCar(Car car) {
        try (ObjectOutputStream o = new ObjectOutputStream(new FileOutputStream(fileToStoreObject))) {
            o.writeObject(car);

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private Car deserializeCar() {
        try (ObjectInputStream in = new ObjectInputStream(new FileInputStream(fileToStoreObject))) {
            return (Car) in.readObject();

        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }
}
