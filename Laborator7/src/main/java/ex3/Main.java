package ex3;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner s = new Scanner(System.in);
        System.out.println("Choose an operation: \n1)Encrypt \n2)Decrypt \nEnter:");
        int choice = s.nextInt();
        switch (choice) {
            case 1:
                EncryptFileService.encryptAndSaveContent("src/main/java/ex3/file.txt");
                break;
            case 2:
                DescryptedFileService.decryptAndSaveContent("src/main/java/ex3/file.txt");
                break;
        }

    }
}

