package ex2;


import java.io.*;
import java.util.Scanner;

public class Main {
    public static void main(String[] args)throws FileNotFoundException {
        int count = 0;
        try {
            File f = new File("src/main/resources/data.txt");
            Scanner reader = new Scanner(f);
            Scanner character = new Scanner(System.in);
            System.out.println("Character is: ");

            char c = character.next().charAt(0);

            while (reader.hasNextLine()) {
                String data = reader.nextLine();
                System.out.println(data);

                for (int i = 0; i < data.length(); i++) {
                    if (data.charAt(i) == c) {
                        count++;
                    }

                }
            }
            reader.close();
            System.out.println("Letter " + c + ", appears " + count + " times in data file!");

        }catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("Letter:" + args[0] + " appears" + count + "timess in data.txt file");
    }
}
