package Ex4;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Random;

public class HomeAutomation {
    public static void main(String[] args) {

        //test using an annonimous inner class
        Home h = new Home() {
            protected void setValueInEnvironment(Event event) {
                System.out.println("New event in environment " + event);
            }

            protected void controllStep() {
                System.out.println("Control step executed");
            }
        };
        h.simulate();
    }
}

abstract class Home {
    private Random r = new Random();
    private final int SIMULATION_STEPS = 20;
    public ControlUnit controlUnit = ControlUnit.getInstance();
    private String fileLocation = "src/main/java/Ex4/";
    private String systemLogName = "system_logs.txt";

    protected abstract void setValueInEnvironment(Event event);

    protected abstract void controllStep();

    private Event getHomeEvent() {
        //randomly generate a new event;
        int k = r.nextInt(100);
        if (k < 30)
            return new NoEvent();
        else if (k < 60)
            return new FireEvent(r.nextBoolean());
        else
            return new TemperatureEvent(r.nextInt(50));
    }

    public void simulate() {
        int k = 0;
        try {
            FileWriter wr = new FileWriter(new File((this.fileLocation + this.systemLogName)));
            while (k < SIMULATION_STEPS) {
                Event event = this.getHomeEvent();
                setValueInEnvironment(event);
                controllStep();
                controlUnit.controlHouse(event, wr);
                Thread.sleep(300);
                k++;
            }
            wr.close();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }
    }

}

abstract class Event {

    EventType type;

    Event(EventType type) {
        this.type = type;
    }

    EventType getType() {
        return type;
    }

}

