package Ex4;


import java.io.FileWriter;
import java.io.IOException;

public class ControlUnit {
    private static volatile ControlUnit instance = null;
    private final TemperatureSensor temperatureSensor = new TemperatureSensor();

    private ControlUnit() {
    }

    public static ControlUnit getInstance() {
        synchronized (ControlUnit.class) {
            if (instance == null) {
                instance = new ControlUnit();
            }
        }
        return instance;
    }

    public void controlHouse(Event event, FileWriter wr) throws IOException {
        if (event.getType() == EventType.FIRE) {
            FireSensor fireSensor = new FireSensor();
            wr.write(fireSensor.activate((FireEvent) event));
        } else if (event.getType() == EventType.TEMPERATURE) {
            wr.write(temperatureSensor.checkTemperature((TemperatureEvent) event));

        } else if (event.getType() == EventType.NONE) {
            wr.write("-------------------------------------------------------\n" +
                    "The house is in good condition and sensors work properly.\n" +
                    "-------------------------------------------------------\n\n");
        }
    }

}

