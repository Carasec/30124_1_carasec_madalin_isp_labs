package Ex4;
public class AlarmUnit {
    boolean checkAlarm;

    public AlarmUnit() {
    }

    public void startAlarm() {
        checkAlarm = true;
    }

    public void stopAlarm() {
        checkAlarm = false;
    }

    @Override
    public String toString() {
        if (checkAlarm) {
            return "Alarm is on!";
        } else {
            return "Alarm is off";
        }
    }
}
