package Ex4;

public class HeatingUnit {
    boolean isHeating;

    public  HeatingUnit(){

    }

    public void startHeating() {
        isHeating = true;
    }

    public void stopHeating() {
        isHeating = false;
    }


    public String toString(int currentValue, int minimValue) {
        if(isHeating){
            return "The temperature is "+currentValue+ ". \nThe heater will start! ";
        }
        else {
            return "The temperature is "+currentValue+". \nThe heater does not have to start! ";
        }
    }
}
