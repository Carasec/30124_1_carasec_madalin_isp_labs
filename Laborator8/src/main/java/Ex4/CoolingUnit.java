package Ex4;

public class CoolingUnit {
    boolean isCooling;

    public void startCooling() {
        isCooling = true;
    }

    public void stopCooling() {
        isCooling = false;
    }

    public String toString(int currentValue, int maximValue) {
        if(isCooling){
            return "The temperature is "+currentValue+ ". \nThe cooling will start! ";
        }
        else {
            return "The temperature is "+currentValue+". \nThe cooling does not have to start! ";
        }
    }
}
