package Ex4;

public class GSMUnit {
    boolean phoneCallStatus;

    public void callOwner(){
        phoneCallStatus = true;
    }

    public void stopCalling(){
        phoneCallStatus = false;
    }

    @Override
    public String toString() {
        if(phoneCallStatus){
            return "Owner is being called!";
        } else{
            return "The phone is ready !";
        }
    }
}
