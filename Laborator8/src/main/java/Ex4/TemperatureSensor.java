package Ex4;

public class TemperatureSensor {
    private final int TEMPERATURE_MIN_VALUE = 25;
    private final int TEMPERATURE_MAX_VALUE = 30;
    private HeatingUnit heatingUnit = new HeatingUnit();
    private CoolingUnit coolingUnit = new CoolingUnit();

    public String checkTemperature(TemperatureEvent temperatureEvent) {
        if (temperatureEvent.getVlaue() < TEMPERATURE_MIN_VALUE) {
            heatingUnit.startHeating();
            coolingUnit.stopCooling();

            return "---------------------------------------------------------\n"+
                    heatingUnit.toString(temperatureEvent.getVlaue(),TEMPERATURE_MIN_VALUE)+
                    "\n---------------------------------------------------------\n\n";

        } else if (temperatureEvent.getVlaue() > TEMPERATURE_MAX_VALUE) {
            heatingUnit.stopHeating();
            coolingUnit.startCooling();

            return "---------------------------------------------------------\n"+
                    coolingUnit.toString(temperatureEvent.getVlaue(), TEMPERATURE_MAX_VALUE)+
                    "\n---------------------------------------------------------\n\n";
        } else {
            heatingUnit.stopHeating();
            coolingUnit.stopCooling();

            return "---------------------------------------------------------\n"+
                    "The temperature is :"+temperatureEvent.getVlaue() +
                    ". \nIt is between "+TEMPERATURE_MIN_VALUE+"-"+TEMPERATURE_MAX_VALUE+
                    "\n---------------------------------------------------------\n\n";
        }
    }
}
