package Cars.Audi;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class AudiSedans extends JFrame implements ItemListener {
    private JLabel label3, label1, label2;
    private JTextArea tArea;

    JFrame f;

    public AudiSedans() {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(550, 480);
        f = new JFrame("Audi Sedans");
        JLabel label = new JLabel();
        label.setHorizontalAlignment(JLabel.CENTER);
        label.setSize(400, 100);
        JButton b = new JButton("Order");
        b.setBounds(140, 160, 75, 20);
        tArea = new JTextArea();
        tArea.setBounds(10, 180, 500, 80);
        String color[] = {" ","Red", "Blue", "Black", "Gray", "White","Green"};
        String model[] = {" ","A4", " A5", "A6", "A7"};
        String engine[] = {" ","40 TDI", "45 TDI", "50 TDI","45 TFSI"};

        final JComboBox cb = new JComboBox(color);
        cb.setBounds(230, 10, 90, 20);

        label1 = new JLabel("Choose your color:");
        label1.setOpaque(true);
        label1.setBounds(10, 10, 200, 20);
        final JComboBox cb1 = new JComboBox(model);
        cb1.setBounds(230, 50, 90, 20);
        label2 = new JLabel("Choose your engine:");
        label2.setOpaque(true);
        label2.setBounds(10, 90, 200, 20);
        final JComboBox cb2 = new JComboBox(engine);
        cb2.setBounds(230, 90, 90, 20);
        label3 = new JLabel("Choose your model:");
        label3.setOpaque(true);
        label3.setBounds(10, 50, 200, 20);
        f.add(cb);
        f.add(cb1);
        f.add(cb2);
        f.add(label2);
        f.add(label1);
        f.add(label3);
        f.add(tArea);
        f.add(b);
        f.setLayout(null);
        f.setSize(350, 350);
        f.setVisible(true);
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        b.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try{
                    if(cb.getSelectedIndex()==0 || cb1.getSelectedIndex()==0 || cb2.getSelectedIndex()==0)
                    {throw new Exception();

                    }
                    String data = "You have ordered a Audi!\n" + "Model: "
                            + cb1.getItemAt(cb1.getSelectedIndex()) + ", Color: " + cb.getItemAt(cb.getSelectedIndex()) + ", Engine:" + cb2.getItemAt(cb2.getSelectedIndex());

                    tArea.setText(data);
                } catch (Exception exception) {
                    tArea.setText("Invalid");
                }

                try {

                    FileWriter writer = new FileWriter("E:\\ISP\\30124_1_carasec_madalin_isp_labs\\Proiect\\src\\main\\java\\Cars\\Audi\\AudiOrdered.txt",true);
                    BufferedWriter writer1=new BufferedWriter(writer);
                    writer1.write("Model: " + cb1.getItemAt(cb1.getSelectedIndex()) + ", Color: " + cb.getItemAt(cb.getSelectedIndex()) + ", Engine: " + cb2.getItemAt(cb2.getSelectedIndex()));
                    writer1.write(" Date: "+dateFormat.format(date)+"\n");
                    writer1.close();
                    FileWriter wr=new FileWriter("E:\\ISP\\30124_1_carasec_madalin_isp_labs\\Proiect\\src\\main\\java\\Cars\\CarsOrdered.txt",true);
                    BufferedWriter bw=new BufferedWriter(wr);
                    bw.write("Car: Audi "+"Model: " + cb1.getItemAt(cb1.getSelectedIndex()) + ", Color: " + cb.getItemAt(cb.getSelectedIndex()) + ", Engine: " + cb2.getItemAt(cb2.getSelectedIndex())+"\n");
                    bw.close();
                    System.out.println("Successfully wrote to the file.");
                } catch (IOException c) {
                    System.out.println("An error occurred.");
                    c.printStackTrace();
                }
            }
        });
    }

    @Override
    public void itemStateChanged(ItemEvent e) {

    }

}
