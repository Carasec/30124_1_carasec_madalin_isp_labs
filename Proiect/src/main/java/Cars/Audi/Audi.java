package Cars.Audi;

import Login.Menu;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Audi extends JFrame {
    JLabel label, label1;

    JButton bVisualize, bVisualize1;

    public Audi() {
        super();
        JLabel background;
        setTitle("Audi");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(550, 400);
        ImageIcon img = new ImageIcon("E:\\audiback.jpg");
        background = new JLabel("", img, JLabel.CENTER);
        background.setBounds(0, 0, 600, 400);
        add(background);
        setVisible(true);
    }


    public void init() {

        this.setLayout(null);
        int width = 150;
        int height = 20;

        bVisualize = new JButton("SUV");
        bVisualize.setBounds(100, 60, width, height);

        bVisualize1 = new JButton("SEDAN");
        bVisualize1.setBounds(350, 60, width, height);

        bVisualize.addActionListener(new Audi.ButtonSuv());

        bVisualize1.addActionListener(new Audi.ButtonSedan());


        ImageIcon car = new ImageIcon();
        label = new JLabel(car, JLabel.CENTER);
        label.setBounds(10, 70, 150, 120);

        ImageIcon Luna = new ImageIcon();
        label1 = new JLabel(Luna, JLabel.CENTER);
        label1.setBounds(200, 70, 150, 120);


        add(bVisualize);
        add(bVisualize1);
        add(label);
        add(label1);

    }


    class ButtonSuv implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            new AudiSuvs();
            this.dispose();
        }

        private void dispose() {
            setVisible(false);
        }
    }

    class ButtonSedan implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            new AudiSedans();
            this.dispose();
        }

        private void dispose() {

            setVisible(false);
        }
    }


}
