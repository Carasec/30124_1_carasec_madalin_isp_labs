package Cars.BMW;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class BMWSedans extends JFrame implements ItemListener {
    private JLabel label3, label1, label2;

    private JTextArea tArea;

    JFrame f;

    public BMWSedans() throws IOException {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(550, 480);
        f = new JFrame("BMW Sedans");
        JLabel label = new JLabel();
        label.setHorizontalAlignment(JLabel.CENTER);
        label.setSize(400, 100);
        JButton b = new JButton("Order");
        b.setBounds(140, 160, 75, 20);
        tArea = new JTextArea(2, 2);
        tArea.setBounds(10, 180, 500, 80);

        String color[] = {" " ,"Red", "Blue", "Black", "Gray", "White"};
        String model[] = {" ","Seria 3", "Seria 4", "Seria 5", "Seria 6"};
        String engine[] = {" ","2.0 D", "2.0 I", "3.0 D", "3.0 I", "5.0 I"};

        final JComboBox cb = new JComboBox(color);
        cb.setBounds(230, 10, 90, 20);

        label1 = new JLabel("Choose your color:");
        label1.setOpaque(true);
        label1.setBounds(10, 10, 200, 20);
        final JComboBox cb1 = new JComboBox(model);
        cb1.setBounds(230, 50, 90, 20);
        label2 = new JLabel("Choose your engine:");
        label2.setOpaque(true);
        label2.setBounds(10, 90, 200, 20);
        final JComboBox cb2 = new JComboBox(engine);
        cb2.setBounds(230, 90, 90, 20);
        label3 = new JLabel("Choose your model:");
        label3.setOpaque(true);
        label3.setBounds(10, 50, 200, 20);
        f.add(cb);
        f.add(cb1);
        f.add(cb2);
        f.add(label2);
        f.add(label1);
        f.add(label3);
        f.add(tArea);
        f.add(b);
        f.setLayout(null);
        f.setSize(350, 350);
        f.setVisible(true);
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        b.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try{
                    if(cb.getSelectedIndex()==0 || cb1.getSelectedIndex()==0 || cb2.getSelectedIndex()==0)
                    {throw new Exception();

                    }
                    String data = "You have ordered a BMW!\n" + "Model: "
                            + cb1.getItemAt(cb1.getSelectedIndex()) + ", Color: " + cb.getItemAt(cb.getSelectedIndex()) + ", Engine:" + cb2.getItemAt(cb2.getSelectedIndex()) +"\n";

                    tArea.setText(data);
                } catch (Exception exception) {
                    tArea.setText("Invalid");
                }

                try {

                    FileWriter writer = new FileWriter("E:\\ISP\\30124_1_carasec_madalin_isp_labs\\Proiect\\src\\main\\java\\Cars\\BMW\\bmwOrdered.txt",true);
                    BufferedWriter writer1=new BufferedWriter(writer);
                    writer1.write("Model: " + cb1.getItemAt(cb1.getSelectedIndex()) + ", Color: " + cb.getItemAt(cb.getSelectedIndex()) + ", Engine: " + cb2.getItemAt(cb2.getSelectedIndex()));
                    writer1.write(" Date: "+dateFormat.format(date)+"\n");
                    writer1.close();
                    FileWriter wr=new FileWriter("E:\\ISP\\30124_1_carasec_madalin_isp_labs\\Proiect\\src\\main\\java\\Cars\\CarsOrdered.txt",true);
                    BufferedWriter bw=new BufferedWriter(wr);
                    bw.write("Car: BMW "+"Model: " + cb.getItemAt(cb.getSelectedIndex()) + ", Color: " + cb1.getItemAt(cb1.getSelectedIndex()) + ", Engine: " + cb2.getItemAt(cb2.getSelectedIndex())+"\n");
                    bw.close();
                    System.out.println("Successfully wrote to the file.");
                } catch (IOException c) {
                    System.out.println("An error occurred.");
                    c.printStackTrace();
                }
            }
        });
    }

    @Override
    public void itemStateChanged(ItemEvent e) {

    }


}




