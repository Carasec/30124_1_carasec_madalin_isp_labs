package Cars.Mercedes;

import Login.Menu;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Mercedes extends JFrame {
    JLabel label, label1;

    JButton bVisualize, bVisualize1;

    public Mercedes() {
        super();
        JLabel background;
        setTitle("Mercedes");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(600, 400);
        ImageIcon img = new ImageIcon("E:\\mercedesbackground.png");
        background = new JLabel("", img, JLabel.CENTER);
        background.setBounds(0, 0, 600, 400);
        add(background);
        setVisible(true);
    }


    public void init() {

        this.setLayout(null);
        int width = 150;
        int height = 20;


        bVisualize = new JButton("SUV");
        bVisualize.setBounds(10, 10, width, height);

        bVisualize1 = new JButton("SEDAN");
        bVisualize1.setBounds(10, 240, width, height);


        bVisualize.addActionListener(new Mercedes.ButtonSuv());


        bVisualize1.addActionListener(new Mercedes.ButtonSedan());


        ImageIcon car = new ImageIcon();
        label = new JLabel(car, JLabel.CENTER);
        label.setBounds(10, 70, 150, 120);

        ImageIcon Luna = new ImageIcon();
        label1 = new JLabel(Luna, JLabel.CENTER);
        label1.setBounds(200, 70, 150, 120);

        add(bVisualize);
        add(bVisualize1);
        add(label);
        add(label1);

    }


    class ButtonSuv implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            new MercedesSuvs();
            this.dispose();
        }

        private void dispose() {
            setVisible(false);
        }
    }

    class ButtonSedan implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            new MercedesSedans();
            this.dispose();
        }

        private void dispose() {

            setVisible(false);
        }
    }


}
