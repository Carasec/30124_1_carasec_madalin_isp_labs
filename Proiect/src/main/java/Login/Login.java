package Login;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import javax.swing.*;

public class Login extends JFrame {

    HashMap accounts = new HashMap();

    JLabel user, pwd;
    JTextField tUser, tPwd;
    JButton bLogin;

    Login() {

        accounts.put("admin", "admin");
        JLabel background;
        setTitle("Login");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(600, 400);
        ImageIcon img = new ImageIcon("E:\\backgroundlogin.png");
        background = new JLabel("", img, JLabel.CENTER);
        background.setBounds(0, 0, 600, 400);
        add(background);
        setVisible(true);
    }

    public void init() {

        this.setLayout(null);
        int width = 80;
        int height = 20;

        user = new JLabel("User ");
        user.setBounds(220, 120, width, height);
        user.setBackground(Color.WHITE);

        pwd = new JLabel("Pasword ");
        pwd.setBounds(190, 160, width, height);
        pwd.setBackground(Color.WHITE);
        tUser = new JTextField();
        tUser.setBounds(250, 120, width, height);

        tPwd = new JTextField();
        tPwd.setBounds(250, 160, width, height);

        bLogin = new JButton("Login");
        bLogin.setBounds(250, 200, width, height);


        bLogin.addActionListener(new TratareButonLogin());

        add(user);
        add(pwd);
        add(tUser);
        add(tPwd);
        add(bLogin);


    }

    public static void main(String[] args) {
        new Login();
    }

    class TratareButonLogin implements ActionListener {

        public void actionPerformed(ActionEvent e) {

            String usr = Login.this.tUser.getText();
            String pwd = Login.this.tPwd.getText();

            if (Login.this.accounts.containsKey(usr)) {
                String correctPwd = (String) Login.this.accounts.get(usr);
                if (correctPwd.equals(pwd)) {
                    Menu.getInstance();
                    this.dispose();
                } else {

                    exit();
                }
            } else {
                exit();
            }

        }

        private void dispose() {
            setVisible(false);
        }
    }

    private void exit() {
    }
}
