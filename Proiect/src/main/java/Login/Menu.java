package Login;

import Cars.Audi.Audi;
import Cars.BMW.BMW;
import Cars.Mercedes.Mercedes;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Menu extends JFrame {

    JButton bBMW, bAUDI, bMERCEDES;

    private static Menu menu = null;

    private Menu() {
        JLabel background;
        setTitle("Menu");
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        init();
        setSize(600, 400);
        ImageIcon img = new ImageIcon("E:\\menubackground1.png");
        background = new JLabel("", img, JLabel.CENTER);
        background.setBounds(0, 0, 600, 400);
        add(background);
        setVisible(true);


    }

    public void init() {
        this.setLayout(null);
        int height = 30;

        bBMW = new JButton("BMW");
        bBMW.setBounds(50, 190, 100, height);
        bBMW.setBackground(Color.GRAY);

        bAUDI = new JButton("AUDI");
        bAUDI.setBounds(240, 190, 100, height);
        bAUDI.setBackground(Color.GRAY);
        bMERCEDES = new JButton("MERCEDES");
        bMERCEDES.setBounds(450, 190, 100, height);
        bMERCEDES.setBackground(Color.GRAY);


        bBMW.addActionListener(new ButtonBMW());
        bAUDI.addActionListener(new ButtonAUDI());
        bMERCEDES.addActionListener(new ButtonMERCEDES());


        add(bBMW);
        add(bAUDI);
        add(bMERCEDES);


    }


    class ButtonBMW implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            new BMW();
            this.dispose();
        }

        private void dispose() {
            setVisible(false);
        }

    }

    class ButtonAUDI implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            new Audi();
            this.dispose();
        }

        private void dispose() {
            setVisible(false);
        }

    }

    class ButtonMERCEDES implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            new Mercedes();
            this.dispose();
        }

        private void dispose() {
            setVisible(false);
        }

    }

    public static Menu getInstance() {
        if (menu == null) {
            menu = new Menu();
        }
        return menu;
    }

}

