package ex4;

public class Controller {
    private static volatile Controller instance = null;
    ex3.Controller.Temperature temperature = new ex3.Controller.Temperature();
    ex3.Controller.Light light = new ex3.Controller.Light();

    public static Controller getInstance() {
        synchronized (Controller.class) {
            if (instance == null) {
                instance = new Controller();
            }
        }
        return instance;
    }

    public void control() {
        try {
            for (int i = 0; i < 20; i++) {
                Thread.sleep(1000);
                System.out.println(("\n\t TempSensor:" + temperature.readValue()));
                System.out.println("\t LightSensor:" + light.readValue());
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        Controller.getInstance().control();
    }
}
