package ex3;

import java.util.Random;

public class Controller {
    public Temperature temperatureSensor = new Temperature();
    public Light lightSensor = new Light();


    public void Control() {
        for (int i = 0; i < 20; i++) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException ex) {
                System.out.println(ex);
            }
            int t = temperatureSensor.readValue();
            int l = lightSensor.readValue();
            System.out.println("Temperature" + t);
            System.out.println("Light" + l);
            System.out.println();
        }
    }

    Temperature[] temperatures = new Temperature[2];
    Light[] lights = new Light[2];

    Controller() {
        temperatures[0] = new Temperature();
        temperatures[1] = new Temperature();
        lights[0] = new Light();
        lights[1] = new Light();

    }

    public static void main(String[] args) {
        Controller controller = new Controller();
    }

    public static class Temperature {
        Random random = new Random();

        public int readValue() {
            return random.nextInt(101);
        }
    }

    public static class Light {
        Random random = new Random();

        public int readValue() {
            return random.nextInt(101);

        }
    }
}
