package ex3;

abstract class Sensor {
    public String location;

    abstract int readValue();

    public String getLocation() {
        return location;
    }

}
