package ex2;

import java.awt.*;

interface Image {
    void display();
}

class RealImage implements Image {

    private String fileName;

    public RealImage(String fileName) {
        this.fileName = fileName;
        loadFromDisk(fileName);
    }

    @Override
    public void display() {
        System.out.println("Displaying " + fileName);
    }

    private void loadFromDisk(String fileName) {
        System.out.println("Loading " + fileName);
    }
}

class RotatedImage implements Image {
    private String filename2;

    public RotatedImage(String filename2) {
        this.filename2 = filename2;
        loadFromDisk(filename2);
    }

    @Override
    public void display() {
        System.out.println("Display rotated" + filename2);
    }

    public void loadFromDisk(String filename2) {
        System.out.println("loading" + filename2);
    }
}


public class Proxy implements Image {

    private Image Image;
    private String fileName;
    boolean chooseImage;


    public Proxy(String fileName, boolean chooseImage) {
        this.fileName = fileName;
        this.chooseImage = chooseImage;
    }

    @Override
    public void display() {
        if (!chooseImage) {


            if (Image == null) {
                Image = new RealImage(fileName);
            }
        } else {

            if (Image == null) {
                Image = new RotatedImage(fileName);
            }
            Image.display();

        }

    }

    public static void main(String[] args) {

    }
}